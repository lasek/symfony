<?php
/**
 * pZ:
 * wykonanie:
 * phpunit -v -c app/
 *
 */

namespace SoftVibe\NowyInteresTestingBundle\Tests\API;

use SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @group ArrayComparator
 */
class ArrayComparatorTest extends WebTestCase
{
    public function testFilledWithEmpty()
    {
        $first = array(
            "key1" => "value",
            "key2" => true,
            "key3" => $this,
        );

        $second = array();

        $comparator = new ArrayComparator($first, $second);

        $compareLine = __LINE__ + 1;
        $result       = $comparator->compare();

        $diffs = $comparator->getDifferences();

        $this->assertFalse($result);
        $this->assertCount(3, $diffs);

        $expectMessage = "\n" . sprintf("Result of comparating arrays in file %s at line %d:", __FILE__, $compareLine);
        $expectMessage .= "\n\n" . "Arrays are not valid";
        $expectMessage .= "\n" . "All differences described below:";

        $expectMessage .= "\n";

        $expectMessage .= "\n" . sprintf("- Key [%s] doesnt exist in API", "key1");
        $expectMessage .= "\n" . sprintf("- Key [%s] doesnt exist in API", "key2");
        $expectMessage .= "\n" . sprintf("- Key [%s] doesnt exist in API", "key3");

        $expectMessage .= "\n";

        $this->assertEquals($expectMessage, $comparator->getMessage());
    }

    public function testEmptyWithFilled()
    {
        $second = array(
            "key1" => "value",
            "key2" => true,
            "key3" => $this,
        );

        $first = array();

        $comparator = new ArrayComparator($first, $second);

        $compareLine = __LINE__ + 1;
        $result       = $comparator->compare();

        $diffs = $comparator->getDifferences();

        $this->assertFalse($result);
        $this->assertCount(3, $diffs);

        $expectMessage = "\n" . sprintf("Result of comparating arrays in file %s at line %d:", __FILE__, $compareLine);
        $expectMessage .= "\n\n" . "Arrays are not valid";
        $expectMessage .= "\n" . "All differences described below:";

        $expectMessage .= "\n";

        $expectMessage .= "\n" . sprintf("- Key [%s] doesnt exist in CSV", "key1");
        $expectMessage .= "\n" . sprintf("- Key [%s] doesnt exist in CSV", "key2");
        $expectMessage .= "\n" . sprintf("- Key [%s] doesnt exist in CSV", "key3");

        $expectMessage .= "\n";

        $this->assertEquals($expectMessage, $comparator->getMessage());
    }

    public function testSameKeys()
    {
        $first = array(
            "key1" => "value",
            "key2" => 1.32432,
            "key3" => 2323,
            "key4" => true
        );

        $second = array(
            "key1" => "changed",
            "key2" => 234.777,
            "key3" => 1211212,
            "key4" => false
        );

        $comparator = new ArrayComparator($first, $second);

        $compareLine = __LINE__ + 1;
        $result       = $comparator->compare();

        $diffs = $comparator->getDifferences();

        $this->assertFalse($result);
        // $this->assertCount(6, $diffs);

        $expectMessage = "\n" . sprintf("Result of comparating arrays in file %s at line %d:", __FILE__, $compareLine);
        $expectMessage .= "\n\n" . "Arrays are not valid";
        $expectMessage .= "\n" . "All differences described below:";

        $expectMessage .= "\n";

        // FIXME: then -> than
        $expectMessage .= "\n" . sprintf("- Key [%s] has other value in CSV than in API:", "key1");
        $expectMessage .= "\n" . sprintf("  CSV value: %s", "value");
        $expectMessage .= "\n" . sprintf("  API value: %s", "changed");

        $expectMessage .= "\n" . sprintf("- Key [%s] has other value in CSV than in API:", "key2");
        $expectMessage .= "\n" . sprintf("  CSV value: %s", "" . (1.32432));
        $expectMessage .= "\n" . sprintf("  API value: %s", "" . (234.777));

        $expectMessage .= "\n" . sprintf("- Key [%s] has other value in CSV than in API:", "key3");
        $expectMessage .= "\n" . sprintf("  CSV value: %s", "" . (2323));
        $expectMessage .= "\n" . sprintf("  API value: %s", "" . (1211212));

        $expectMessage .= "\n" . sprintf("- Key [%s] in CSV has value TRUE but in API it's FALSE", "key4");

        $expectMessage .= "\n";

        $this->assertEquals($expectMessage, $comparator->getMessage());
    }


    public function testArrayInArray()
    {
        $first = array(
            "array" => array(
                "key1"   => "value",
                "key2"   => 1.32432,
                "key3"   => 2323,
                "key4"   => true,
                "array2" => array()
            )
        );

        $second = array(
            "array" => array(
                "key1"   => "changed",
                "key2"   => 234.777,
                "key3"   => 1211212,
                "key4"   => false,
                "array2" => array()

            )
        );

        $comparator = new ArrayComparator($first, $second);

        $compareLine = __LINE__ + 1;
        $result       = $comparator->compare();

        $diffs = $comparator->getDifferences();

        $this->assertFalse($result);
        // $this->assertCount(6, $diffs);

        $expectMessage = "\n" . sprintf("Result of comparating arrays in file %s at line %d:", __FILE__, $compareLine);
        $expectMessage .= "\n\n" . "Arrays are not valid";
        $expectMessage .= "\n" . "All differences described below:";

        $expectMessage .= "\n";

        $expectMessage .= "\n" . sprintf("- Key [array][%s] has other value in CSV than in API:", "key1");
        $expectMessage .= "\n" . sprintf("  CSV value: %s", "value");
        $expectMessage .= "\n" . sprintf("  API value: %s", "changed");

        $expectMessage .= "\n" . sprintf("- Key [array][%s] has other value in CSV than in API:", "key2");
        $expectMessage .= "\n" . sprintf("  CSV value: %s", "" . (1.32432));
        $expectMessage .= "\n" . sprintf("  API value: %s", "" . (234.777));

        $expectMessage .= "\n" . sprintf("- Key [array][%s] has other value in CSV than in API:", "key3");
        $expectMessage .= "\n" . sprintf("  CSV value: %s", "" . (2323));
        $expectMessage .= "\n" . sprintf("  API value: %s", "" . (1211212));

        $expectMessage .= "\n" . sprintf("- Key [array][%s] in CSV has value TRUE but in API it's FALSE", "key4");

        $expectMessage .= "\n";

        $this->assertEquals($expectMessage, $comparator->getMessage());
    }

    /**
     * pZ: testA_pZ
     * Nazwa do zmiany
     */
    public function testA_pZ()
    {
        $googleExport = array(
            1 => array(
                2012 => array(
                    'sum' => array(
                        'XI'  => 15,
                        'XII' => 43,
                    ),
                ),
            ),
            2 => array(
                2013 => array(
                    'sum' => array(
                        'Suma'  => 6789,
                    ),
                ),
            ),
        );

        $niResult = array(
            1 => array(
                2012 => array(
                    'sum' => array(
                        'XII' => 45,
                    ),
                ),
            ),
            2 => array(
            ),
        );

        $comparator = new ArrayComparator($googleExport, $niResult);

        $result = $comparator->compare();

        $diffs = $comparator->getDifferences();

        $out = $comparator->getMessage();

//        print_r($out); die(' -print');
    }
}
