<?php
/**
 * pZ:
 * wykonanie:
 * phpunit -v -c app/ src\SoftVibe\NowyInteresTestingBundle\Tests\API\EmployeeParserTest.php
 *
 */

namespace SoftVibe\NowyInteresTestingBundle\Tests\API;

use SoftVibe\NowyInteresTestingBundle\Tests\API\Parser\EmployeeParser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparator;

/**
 * @group EmployeeParser
 */
class EmployeeParserTest extends WebTestCase
{
    public function testEmployee()
    {
        $parser = new EmployeeParser("1.csv");

        $employees = $parser->parse();

//        $w = print_r($employees, true);
//        file_put_contents('w.txt', $w);

        $this->assertCount(5, $employees, "W pliku CSV MUSI byc 4 pracownikow + suma");
        $this->assertEquals('Pomocnik \ "YAP"', $employees[1]['nazwa']);
        $this->assertEquals(13000, $employees[1]['ustawienia']['wynagrodzenie']);
        $this->assertEquals(strtotime("01-05-2013"), $employees[1]['ustawienia']['poczatek']);
        $this->assertEquals(5, $employees[1]['ustawienia']['wzrost']);
        
        $bpWzor = $employees[0]['bp_wzor'];

        $this->assertCount(3, $bpWzor, "MUSZA byc 3 sekcje");
        $this->assertCount(1, $bpWzor[1], "W pierwszej sekcji MUSI byc JEDEN rok");
        $this->assertCount(1, $bpWzor[2], "Druga sekcja MUSI miec juz tylko jeden rok");
        $this->assertCount(3, $bpWzor[3], "Lata w trzeciej sekcji MUSZA byc 3");
//        $this->assertCount(10, $bpWzor[1][2013]["sum"], "8 miesięcy w pierwszym roku opisane szczegolowo");
//        $this->assertEquals(14050, $bpWzor[1][2013]["sum"]["VII"], "szczegółowe sprawdzanie");
//        $this->assertEquals(37850, $bpWzor[2][2014]["wynagrodzenia"]["VIII"], "szczegółowe sprawdzanie");
    }

    public function nie_testDirectlyFromGoogleDocs()
    {

        $parserOpenoffice = new EmployeeParser("170714.coverted.from.open.office.docs.csv");
        $parserGoogle     = new EmployeeParser("170714.directly.from.google.docs.csv");

        $employeesOpenoffice = $parserOpenoffice->parse();
        $employeesGoogle     = $parserGoogle->parse();

        $comparator = new ArrayComparator($employeesOpenoffice, $employeesGoogle);

        $comparator->compare();

        $diffs = $comparator->getDifferents();

        print $comparator->getMessage();
    }
}