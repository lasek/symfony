<?php

namespace SoftVibe\NowyInteresTestingBundle\Tests\API\Parser;

class Parser
{
    static $DATASHEET_DIR = "../../../Resources/DataSheet";

    static $MONTH_TABLE = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");

    static $EXCEL_MONTHS_TABLE = array(1 => "sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paź", "lis", "gru");

    private $file;
    protected $data = null;
    protected $bpStart;

    public function __construct($file = "", $moduleDir = null)
    {
        $path = realpath(dirname(__FILE__) . "/" . self::$DATASHEET_DIR);

        if (null !== $moduleDir) {
            $path .= "/" . $moduleDir;
        }

        $path .= "/" . $file;

        $this->file = $path;
    }

    public function parse()
    {
        if (!file_exists($this->file)) {
            throw new ParserException("Plik nie istnieje");
        }

        $content = file_get_contents($this->file);

        // parsowanie CSV wiersz po wierszu
        $rows = explode("\n", $content);
        $data = array();

        foreach ($rows as $row) {
            $data[] = str_getcsv($row, ",", '"', '"');
        }

        $this->data = $data;

        return $data;
    }

    private function parseType($value, $type)
    {

        switch ($type) {
            case 'int':
                $value = $this->parseToInt($value);
                break;

            case 'date':
                $value = $this->getTimestamp($value);
                break;
            
            default:
                break;
        }

        return $value;
    }

    protected function parseSchemeSimple($settingsSection, $sections = null)
    {
        $items = array();

        if (null !== $sections) {
        
            foreach ($sections as &$section) {

                $section['row_start'] = $this->searchSection($section['name']);
            }
        }

        $settingsSectionStartRow = $this->searchSection($settingsSection['name']);
        $datesHeadersRow = $this->data[$settingsSection['dates_row']];


        $i = 1;
        $additionalLoop = 0;


        while ( !empty($this->data[$i + $settingsSectionStartRow][1]) || 0 === $additionalLoop++) {
            $settingsRow = $this->data[$i + $settingsSectionStartRow];

            $params = array();

            foreach ($settingsSection['settings_map'] as $setting) {

                $value = $settingsRow[$setting['column_index']];

                $value = $this -> parseType($value, $setting['type']);

                $params[$setting['param_name']] = $value;

            }

            $col = 3;

            if (null !== $this->bpStart) {

                while ($col < count($datesHeadersRow)) {

                    $timestamp = $this->getTimestamp($datesHeadersRow[$col]);

                    if ($this->bpStart <= $timestamp) {

                        break;
                    }

                    $col ++;
                }
            }

            $bpWzor = array(
                1 => array(),
                2 => array(),
                3 => array()
            );

            $stage = 1;

            $actualYear = date("Y", $this->getTimestamp($datesHeadersRow[$col]));

            while ( !empty($datesHeadersRow[$col]) ) {

                $dateCell = $datesHeadersRow[$col];

                $dateInt = intval($dateCell);

                // Jedyny chyba sposób na wykrycie kolumny z samą datą..
                if ($dateInt > 1900 && $dateInt < 4000) {
                    $currentYear  = $dateInt;
                    $currentMonth = "Suma";
                    $stage        = 3;
                } else {

                    $currentDate = $this->getTimestamp($dateCell);

                    $currentYear = date("Y", $currentDate);

                    if ($actualYear != $currentYear) {
                        $stage = 2;
                    }

                    $currentMonth = Parser::$MONTH_TABLE[intval(date("m", $currentDate))];
                }

                if (null !== $sections) {

                    foreach ($sections as &$section) {
                        $sectionRowItem = $section['row_start'] + $i;
                        $itemRow = $this->data[$sectionRowItem];
                        $value = $this -> parseType($itemRow[$col], $section['type']);

                        $bpWzor[$stage][$currentYear][$section['bp_name']][$currentMonth] = $value;
                    }
                }

                $col++;
            }

            $data = array(
                "ustawienia" => $params,
                "nazwa"      => $params['nazwa'],
                "bp_wzor"    => $bpWzor,
            );

            if(0 !== $additionalLoop) {

                $items["suma"] = $data;
            } else {

                $items[] = $data;

            }

            $i++;
        }

        return $items;
    }

    protected function searchSection($sectionName)
    {
        foreach ($this->data as $id => $row) {
            if ($row[0] == $sectionName) {
                break;
            }
        }

        return $id;
    }

    /**
     * pZ: getTimestamp
     * Funkcja parsuje daty zapisane w CSV do timestamp
     *
     * @param $xlsStamp Liczba zapisana w string
     *
     * @return int
     */
    protected static function getTimestamp($xlsStamp)
    {
        if (empty($xlsStamp)) {
            return 0;
        }

        if (preg_match("/^(\d\d?)-(.{3})-(\d{4})$/u", $xlsStamp, $params)) {

            $month = array_search($params[2], self::$EXCEL_MONTHS_TABLE);

            $formattedData = sprintf("%02s-%02s-%s", $params[1], $month, $params[3]);
        } else {
            $xlsStamp   = intval($xlsStamp);
            $pseudoUnix = ($xlsStamp - 25569) * 86400;

            $formattedData = date('d-m-Y', $pseudoUnix);
        }

        return strtotime($formattedData);
    }

    /**
     * Funkcja parsuje liczby zapisane w string z CSV do formatu float
     * pZ: chcemy poprawiać zapis w CSV taki: "40,5%" na bez procenta i z kropką (poprawny float)
     *
     * @param string $val Liczba zapisana w string
     *
     * @return float
     */
    protected static function parseToInt($val)
    {
        $params = null;
        // Usunięcie tzw twardej spacji Unicode (C2A0)
        $val = preg_replace('/(\xc2\xa0)|\s/', '', $val);

        $pattern = "/^(\\-?[\\d\\s]+([\\.,]\d+)?)\\s?(\\%?)(zł)?$/u";

        if (preg_match($pattern, $val, $params)) {

            $validCommaStr = str_replace(",", ".", $params[1]);

            $intVal = floatval($validCommaStr);
        }

        return (isset($intVal) ? $intVal : '0');
    }
}
