<?php

namespace SoftVibe\NowyInteresTestingBundle\Tests\API\Parser;

class LoanParser extends Parser {

    /**
     * Konstruktor
     *
     * @param string $file
     * @param int    $bpStart data startu biznes planu
     */
    public function __construct($file, $bpStart = null)
    {
        parent::__construct($file, "Loan");

        $this->bpStart = $bpStart;
    }

    /**
     * Parsowanie wyników
     *
     * @return array lista pracowników
     */
    public function parse()
    {
        $loans = array();

        parent::parse();

        $settingsSection = array(
            "name" => "Ustawienia",
            "dates_row" => $this->searchSection("Numerowanie") - 1,

            "settings_map" => array(
                array(
                    "param_name" => "nazwa",
                    "column_index" => 1,
                    "type" => "raw",
                ),
                array(
                    "param_name" => "wartosc",
                    "column_index" => 2,
                    "type" => "int",
                ),
                array(
                    "param_name" => "okres",
                    "column_index" => 3,
                    "type" => "int",
                ),
                array(
                    "param_name" => "poczatek",
                    "column_index" => 4,
                    "type" => "date",
                ),
                array(
                    "param_name" => "koniec",
                    "column_index" => 5,
                    "type" => "date",
                ),
                array(
                    "param_name" => "oprocentowanie",
                    "column_index" => 6,
                    "type" => "int",
                ),
                array(
                    "param_name" => "oprocentowanie_miesieczne",
                    "column_index" => 7,
                    "type" => "int",
                ),

            )
        );

        $sections = array(
            array(
                "name" => "Odsetki",
                "bp_name" => "odsetki",
                "type" => "int"
            ),

            array(
                "name" => "Odsetki",
                "bp_name" => "sum",
                "type" => "int"
            )
        );

        return $this->parseSchemeSimple($settingsSection, $sections);
    }

}