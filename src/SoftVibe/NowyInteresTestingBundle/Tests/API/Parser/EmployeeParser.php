<?php

namespace SoftVibe\NowyInteresTestingBundle\Tests\API\Parser;

class EmployeeParser extends Parser {

    /**
     * Konstruktor
     *
     * @param string $file
     * @param int    $bpStart data startu biznes planu
     */
    public function __construct($file, $bpStart = null)
    {
        parent::__construct($file, "Employee");

        $this->bpStart = $bpStart;
    }

    /**
     * Parsowanie wyników
     *
     * @return array lista pracowników
     */
     /**
     * Parsowanie wyników
     *
     * @return array lista pracowników
     */
    public function parse()
    {
        $loans = array();

        parent::parse();

        $settingsSection = array(
            "name" => "Ustawienia",
            "dates_row" => $this->searchSection("Wynagrodzenie") - 1,

            "settings_map" => array(
                array(
                    "param_name" => "nazwa",
                    "column_index" => 1,
                    "type" => "raw",
                ),
                array(
                    "param_name" => "wynagrodzenie",
                    "column_index" => 2,
                    "type" => "int",
                ),
                array(
                    "param_name" => "poczatek",
                    "column_index" => 3,
                    "type" => "date",
                ),
                array(
                    "param_name" => "koniec",
                    "column_index" => 4,
                    "type" => "date",
                ),
                array(
                    "param_name" => "wzrost",
                    "column_index" => 5,
                    "type" => "int",
                ),
                array(
                    "param_name" => "koszt",
                    "column_index" => 6,
                    "type" => "int",
                ),

            )
        );

        $sections = array(
            array(
                "name" => "Wynagrodzenie",
                "bp_name" => "wynagrodzenia",
                "type" => "int"
            ),

            array(
                "name" => "Premie",
                "bp_name" => "premie",
                "type" => "int"
            ),

            array(
                "name" => "Inne płacowe",
                "bp_name" => "inne",
                "type" => "int"
            ),

            array(
                "name" => "Wyliczenie kosztów pracy",
                "bp_name" => "koszty",
                "type" => "int"
            ),

            array(
                "name" => "Całkowity koszt zatrudnienia",
                "bp_name" => "sum",
                "type" => "int"
            )
        );

        return $this->parseSchemeSimple($settingsSection, $sections);
    }
}