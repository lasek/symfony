<?php

namespace SoftVibe\NowyInteresTestingBundle\Tests\API;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use SoftVibe\NowyInteresTestingBundle\Tests\API\Parser\Parser;

class FinanceTest extends WebTestCase
{
//    static $HOST = 'http://dev.nowyinteres.pl';
    static $HOST = 'http://business2.localhost/app_dev.php';

    protected function parseDate($timestamp)
    {
        return date("Y-m-d", $timestamp);
    }

    protected function callURL($post, $JSON = true)
    {
        $token = 'f1290186a5d0b1ceab27f4e77c0c5d68';

        $url = self::$HOST . '/api/' . $token . '/result';

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (is_array($post)) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        $output = curl_exec($ch);

        $err = curl_error($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        if ($JSON) {
            return json_decode($output, true);
        }

        return $output;
    }

    protected function getCsvFiles($moduleName)
    {

        $dir = realpath(dirname(__FILE__) . "/Parser/" . Parser::$DATASHEET_DIR) . "/" . $moduleName;

        $csvFiles = array();

        if (is_dir($dir)) {

            $files = scandir($dir);

            foreach ($files as $file) {

                // wzor: {index}_{plan_start_date}_{plan_forecasting}_{plan_detail_years}
                // nie zamykam regexpr ($) zeby mozna bylo wstawic sobie jakis komentarz lub tag
                // w nazwie pliku, np:
                // 1_2014-02-01_3_2_poprawiony.csv
                $pattern = '/^(\d+?)_(\d{4}-\d{2}-\d{2})_(\d+?)_(\d+?)/';

                if (preg_match($pattern, $file, $params)) {

                    $csvFiles[] = array(
                        "filename"          => $file,
                        "index"             => intval($params[1]),
                        "plan_start_date"   => $params[2],
                        "plan_forecasting"  => intval($params[3]),
                        "plan_detail_years" => intval($params[4]),
                    );
                }
            }
        }

        usort($csvFiles, function ($a, $b) {
            return $a['index'] - $b['index'];
        });

        return $csvFiles;
    }
}