<?php
/**
 * pZ:
 * wykonanie:
 * phpunit -v -c app/ src\SoftVibe\NowyInteresTestingBundle\Tests\API\EmployeeTest.php
 * phpunit -v -c app/ --group Employee
 */

namespace SoftVibe\NowyInteresTestingBundle\Tests\API;

use SoftVibe\NowyInteresTestingBundle\Tests\API\Parser\Parser;
use SoftVibe\NowyInteresTestingBundle\Tests\API\Parser\EmployeeParser;
use SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparator;

/**
 * Class EmployeeTest
 *
 * @group Employee
 */
class EmployeeTest extends FinanceTest
{
    protected function getBpFromAPI($params, $index = "01")
    {
        // $p = array(
        //     'salary'        => 8000, // integer
        //     'floatingPrice' => 5, // % - integer
        //     'labourCosts'   => 20, // % - integer
        //     'startAt'       => '2014-11-01', // 4 cyfry '-' 2 cyfry '-' 2 cyfry lub 'null'
        //     'endAt'         => 'null', // 4 cyfry '-' 2 cyfry '-' 2 cyfry lub 'null'
        // );
        //http://dev.nowyinteres.pl/sdata/{salary}/{floatingPrice}/{labourCosts}/{startAt}/{endAt}/{employee_id}/result
        $url = sprintf('%s/sdata/%d/%s/%s/%s/%s/%02d/result',
            FinanceTest::$HOST,
            $params['wynagrodzenie'],
            $params['wzrost'],
            $params['koszt'],
            $this->parseDate($params['poczatek']),
            'null',
            $index);

        $post = array(
            'module'        => 'employee',
            'action'        => 'update',

            'index'         => $index,
            'salary'        => $params['wynagrodzenie'],
            'floatingPrice' => $params['wzrost'],
            'labourCosts'   => $params['koszt'],
            'startAt'       => $this->parseDate($params['poczatek']),
        );

        return $this->callURL($post);
    }


    /**
     * Pierwszy test porównawczy
     * Na razie zawieszony z powodu takiego, że następny test dodaje 3-ech pracownikow
     * I nie da sie ich usunac
     *
     * @return void
     */
    public function no_testEmployees()
    {
        // pZ: ustawienia planu - napisać to całkiem inaczej : zmienne, lokalizacja, etc ....
        // nazwy takie jak mam w bazie danych (bez przedrostka 'plan_')
        $plan_start_date   = '2013-02-01';
        $plan_forecasting  = 3;
        $plan_detail_years = 2;

        $parser = new EmployeeParser("170714.directly.from.google.docs.csv", strtotime($plan_start_date));

        $employees = $parser->parse();

        $employee = $employees[0];

        $out = $this->getBpFromAPI($employee['ustawienia']);

        // $this->assertNotNull($out, sprintf("Error API (url: %s)", $url));

        // pZ: wykonaj: phpunit -v -c app/ > wynik_z_ni.txt
        // print_r($out); die(' -out');

        $comparator = new ArrayComparator($employee['bp_wzor'], $out);

        $result = $comparator->compare();

//        file_put_contents('1.txt', print_r($out, true));
//        file_put_contents('2.txt', print_r($employee['bp_wzor'], true));

        print $comparator->getMessage();

        $this->assertTrue($result, "bp_wzory musza sie zgadzac");
    }

    public function testScanFolder()
    {
        $csvFiles = $this->getCsvFiles("Employee");

        foreach ($csvFiles as $csv) {

            $parser = new EmployeeParser($csv['filename'], strtotime($csv['plan_start_date']));

            $employees = $parser->parse();

            foreach ($employees as $index => $employee) {
                if ($index !== 'suma') {
                    $out = $this->getBpFromAPI($employee['ustawienia'], $index);
                }
            }

            $this->assertNotNull($out, 'Błąd api');
            $this->assertInternalType("array", $out, 'message');
//            print_r($out); die('-out');

            $suma = $employees['suma']['bp_wzor'];
//            print_r($suma); die('-suma');

            $comparator = new ArrayComparator($suma, $out);

            $comparator->setRound(0);

            $result = $comparator->compare();

            echo $comparator->getMessage();
            $this->assertTrue($result, sprintf("porównywanie bp_wzoru z pliku '%s'", $csv['filename']));
        }
    }

    public function no_testPostAPI()
    {
        $url = FinanceTest::$HOST . "/sdata";

        $data = array(
            'module'            => 'employee',
            'action'            => 'add',

            'plan_start_date'   => '2013-01-01',
            'plan_forecasting'  => 3,
            'plan_detail_years' => 2,

            'salary'            => 8000,
            'floatingPrice'     => 5,
            'labourCosts'       => 20,
            'startAt'           => '2014-11-01',
            'endAt'             => 'null',
        );

        $result = $this->callURL($data);
        // $this->assertInternalType("array", $result);
    }
}