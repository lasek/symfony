<?php

namespace SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator;

use SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparatorDifference\Difference;
use SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparatorDifference\DifferenceStrict;
use SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparatorDifference\DoesntExistDifference;
use SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparatorDifference\DoesntExistInGridDifference;
use SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparatorDifference\ArrayDifference;

class ArrayComparator
{
    protected $gridArray = null;
    protected $compareArray = null;

    protected $result = null;
    protected $diffs = null;
    protected $callPoint = null;
    protected $roundPrecision = null;

    /**
     * Konstruktor klasy
     *
     * @param array [$gridArray] pierwsza tablica
     * @param array [$compareArray]   druga tablica
     */
    public function __construct($gridArray, $compareArray)
    {
        $this->gridArray    = $gridArray;
        $this->compareArray = $compareArray;
        $this->msg          = '';
    }

    private function walk($gridArray, $comparingArray)
    {
        $diffs = array();

        foreach ($gridArray as $key => $value) {

            if (!array_key_exists($key, $comparingArray)) {

                $diffs[] = new DoesntExistDifference($key);
                continue;
            }

            $compareValue = $comparingArray[$key];

            if ((gettype($value) == gettype($compareValue))) {
                if (gettype($compareValue) == "array") {

                    $internalDiffs = $this->walk($value, $compareValue);

                    if (0 !== count($internalDiffs)) {
                        $diffs[] = new ArrayDifference($key, $internalDiffs);
                    }

                    continue;
                }
            }

            if (
                in_array(gettype($value), array("double", "integer", "string")) &&
                in_array(gettype($compareValue), array("double", "integer", "string"))
            ) {

                if (gettype($value) == "string" && gettype($compareValue) != "string") {

                    $value = floatval($value);
                } else if (gettype($compareValue) == "string" && gettype($value) != "string") {

                    $compareValue = floatval($compareValue);
                }

                if (null !== $this->roundPrecision) {

                    // pZ: wartości w tablicy jaką mamy z API zaokrąglamy tak jakbyśmy chcieli jej wartości
                    // wyświetlić w Gridach albo dać do tabel w PDF - czyli bez groszy
                    // oczywiście jakbyśmy używali tych wartości do obliczeń to zaokrąglenia nie robilibyśmy
                    if (in_array(gettype($compareValue), array("double", "integer"))) {
                        $compareValue = round($compareValue, $this->roundPrecision, PHP_ROUND_HALF_UP);
                    }
                }

                if ($value != $compareValue) {
                    $diffs[] = new Difference($key, $value, $compareValue);
                }

                continue;
            }

            if ($value !== $compareValue) {
                $diffs[] = new DifferenceStrict($key, $value, $compareValue);
            }
        };

        foreach ($comparingArray as $key => $value) {

            if (!array_key_exists($key, $gridArray)) {
                $difference = new DoesntExistInGridDifference($key);
            } else {
                continue;
            }

            $diffs[] = $difference;
        };

        return $diffs;
    }

    /**
     * Rozpoczyna porównywanie tablic
     *
     * @return bool zwraca true jeśli tablice są identyczne
     */
    public function compare()
    {
        $this->diffs = array();

        $backTrace = debug_backtrace();

        $this->callPoint = array_shift($backTrace);

        $this->diffs = $this->walk($this->gridArray, $this->compareArray);

        $this->result = !count($this->diffs);

        return $this->result;
    }

    private function strictDescription($value) {
        if (null === $value) return "NULL";
        if (true === $value) return "TRUE";
        if (false === $value) return "FALSE";

        if(gettype($value) == "object") return sprintf("instanceof %s();", get_class($value));
        if(gettype($value) == "string") return sprintf("string(%d)[%s]", strlen($value), $value);
        if(gettype($value) == "integer") return sprintf("integer(%d)", ($value));
        if(gettype($value) == "double") return sprintf("double(%d)", ($value));
        if(gettype($value) == "array") return sprintf("array(%d)", count($value));
    }

    private function generateDifferenceMessages($diffs = null, $keyHistory = '')
    {
        $msg = "";

        if (null === $diffs) {
            $diffs = $this->diffs;
        }

        foreach ($diffs as $diff) {

            if ($diff instanceof ArrayDifference) {
                $msg .= $this->generateDifferenceMessages($diff->diffsList, sprintf("%s[%s]", $keyHistory, $diff->key));
            } else if ($diff instanceof DoesntExistInGridDifference) {

                $msg .= "\n" . sprintf("- Key %s[%s] doesnt exist in CSV", $keyHistory, $diff->key);
            } else if ($diff instanceof DoesntExistDifference) {

                $msg .= "\n" . sprintf("- Key %s[%s] doesnt exist in API", $keyHistory, $diff->key);
            } else if ($diff instanceof DifferenceStrict) {

                $msg .= "\n" . sprintf("- Key %s[%s] in CSV has value %s but in API it's %s",
                        $keyHistory, $diff->key,
                        $this->strictDescription($diff->gridValue),
                        $this->strictDescription($diff->comparingValue));
            } else if ($diff instanceof Difference) {

                $msg .= "\n" . sprintf("- Key %s[%s] has other value in CSV than in API:", $keyHistory, $diff->key);
                $msg .= "\n" . sprintf("  CSV value: %s", "" . $diff->gridValue);
                $msg .= "\n" . sprintf("  API value: %s", "" . $diff->comparingValue);
            }
        }

        return $msg;
    }

    public function getMessage()
    {
        $msg = "";

        if (!$this->result) {

            $msg .= "\n" . sprintf("Result of comparating arrays in file %s at line %d:", $this->callPoint['file'], $this->callPoint['line']);
            $msg .= "\n";

            $msg .= "\n" . "Arrays are not valid";
            $msg .= "\n" . "All differences described below:";
            $msg .= "\n";

            $msg .= $this->generateDifferenceMessages();
            $msg .= "\n";
        }

        return $msg;
    }

    public function getDifferences()
    {
        return $this->diffs;
    }

    public function setRound($precision)
    {
        $this->roundPrecision = $precision;
    }
}
