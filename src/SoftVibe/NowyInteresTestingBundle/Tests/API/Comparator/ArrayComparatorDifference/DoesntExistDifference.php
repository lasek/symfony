<?php

namespace SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparatorDifference;

class DoesntExistDifference extends Difference
{

    public function __construct($key)
    {
        $this->key = $key;
    }
}