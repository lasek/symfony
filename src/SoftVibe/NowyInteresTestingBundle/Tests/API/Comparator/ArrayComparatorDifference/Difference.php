<?php

namespace SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparatorDifference;

class Difference
{
    public $key;
    public $gridValue;
    public $comparingValue;

    public function __construct($key, $gridValue, $comparingValue)
    {
        $this->key = $key;

        $this->gridValue = $gridValue;

        $this->comparingValue = $comparingValue;
    }
}