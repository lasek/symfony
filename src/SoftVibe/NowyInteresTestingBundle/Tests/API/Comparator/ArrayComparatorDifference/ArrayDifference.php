<?php

namespace SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparatorDifference;

class ArrayDifference extends Difference
{
    public $diffsList;

    public function __construct($key, $diffsList)
    {
        $this->key = $key;

        $this->diffsList = $diffsList;
    }
}