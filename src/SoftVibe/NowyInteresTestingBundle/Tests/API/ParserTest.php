<?php
/**
 * pZ: test samego Parsera
 * wykonanie:
 * phpunit -v -c app/ --group Parser
 */

namespace SoftVibe\NowyInteresTestingBundle\Tests\API;

use SoftVibe\NowyInteresTestingBundle\Tests\API\Parser\Parser;

/**
 * Class EmployeeTest
 *
 * @group Parser
 */
class ParserTest extends FinanceTest
{
    private $parser;

    protected function setUp()
    {
        $this->parser = new dumpParserClass();
    }

    public function testParserMethod_parseToInt()
    {
        // pZ: procenty
        $this->assertEquals(34.6, $this->parser->getResultFrom_parseToInt('34,6%'), 'opis 1');
        $this->assertEquals(4.3, $this->parser->getResultFrom_parseToInt(' 4,30% '), 'opis 2 - moga byc spacje');
        $this->assertEquals(2.23, $this->parser->getResultFrom_parseToInt('2.23%'), 'opis 3');
        $this->assertEquals(-2.1, $this->parser->getResultFrom_parseToInt('-2.10%'), 'opis 4');
        $this->assertEquals(27, $this->parser->getResultFrom_parseToInt('27%'), 'opis 5');

        $this->assertEquals(9, $this->parser->getResultFrom_parseToInt('9 %'), 'opis 6 (spacja przed procentem');

        // pZ: złotówki
        $this->assertEquals(34456, $this->parser->getResultFrom_parseToInt('34 456 zł'), '');

        // pZ: inne
        $this->assertEquals(9.21334, $this->parser->getResultFrom_parseToInt('9,21334'), 'parsowanie zwykłej liczby zmiennoprzecinkowej');
        $this->assertEquals(456.3, $this->parser->getResultFrom_parseToInt(456.3), 'Liczba jako argument');
    }

    private function checkCustomDate($in, $out, $msg = '')
    {
        $result = $this->parser->getResultFrom_getTimestamp($in);
        $origin = date('Y-m-d', $result);

//        echo $result . ' - ' . $origin . PHP_EOL;

        $this->assertEquals($origin, $out, $msg);
    }

    public function testParserMethod_getTimestamp()
    {
        $this->checkCustomDate('3-sty-2014', '2014-01-03');
        $this->checkCustomDate('31-sty-2014', '2014-01-31');
        $this->checkCustomDate('19-gru-1987', '1987-12-19');
        $this->checkCustomDate('24-paź-2011', '2011-10-24');
        $this->checkCustomDate('1-mar-2036', '2036-03-01');
        $this->checkCustomDate('1-sty-2038', '2038-01-01');

        $this->checkCustomDate('19-sty-2038', '2038-01-19');

        $this->checkCustomDate('41365', '2013-04-01', 'ERROR 1: to data z OpenOffice(Excel) - do poprawki!');
        $this->checkCustomDate('41395', '2013-05-01', 'ERROR 2: to data z OpenOffice(Excel) - do poprawki!');
        $this->checkCustomDate('41487', '2013-08-01', 'ERROR 3: to data z OpenOffice(Excel) - do poprawki!');
        // pZ: dodałem do jeden liczbę 23 dni czyli dało 24
        $this->checkCustomDate(41487 + 23, '2013-08-24', 'ERROR 4: to data z OpenOffice(Excel) - do poprawki!');
    }
}

/**
 * Class dumpParserClass
 *
 * Potrzebna aby wywoływać metody protected
 */
class dumpParserClass extends Parser
{
    public function getResultFrom_parseToInt($val)
    {
        return $this->parseToInt($val);
    }

    public function getResultFrom_getTimestamp($val)
    {
        return $this->getTimestamp($val);
    }
}
