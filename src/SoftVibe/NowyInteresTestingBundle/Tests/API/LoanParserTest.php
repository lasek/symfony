<?php
/**
 * pZ:
 * wykonanie:
 * phpunit -v -c app/ src\SoftVibe\NowyInteresTestingBundle\Tests\API\EmployeeParserTest.php
 *
 */

namespace SoftVibe\NowyInteresTestingBundle\Tests\API;

use SoftVibe\NowyInteresTestingBundle\Tests\API\Parser\LoanParser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use SoftVibe\NowyInteresTestingBundle\Tests\API\Comparator\ArrayComparator;

/**
 * @group EmployeeParser
 */
class LoanParserTest extends WebTestCase
{
    public function testLoan()
    {
        $parser = new LoanParser("first.csv");

        $loans = $parser->parse();

        $this->assertCount(3, $loans, 'W systemie są 2 pożyczki + suma');

        $this->assertEquals(2500, $loans[0]['bp_wzor'][1][2013]['odsetki']['V']);
        $this->assertEquals(0, $loans[1]['bp_wzor'][1][2013]['odsetki']['V']);
        $this->assertEquals(2500, $loans['suma']['bp_wzor'][1][2013]['odsetki']['V']);

    }
}
