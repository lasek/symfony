<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml"
    xmlns:dbx="http://phpdoc.org/xsl/functions"
    exclude-result-prefixes="dbx">
  <xsl:output indent="yes" method="html" />

  <xsl:template match="method/name">
    <h4 class="method {../@visibility}">
      <img src="{$root}images/icons/visibility_{../@visibility}.png" style="margin-right: 10px" alt="{@visibility}" title="{@visibility}"/>
      <xsl:value-of select="." />
    </h4>
      <div class="to-top"><a href="#{../../name}">jump to class</a></div>
  </xsl:template>

  <xsl:template match="function/name">
    <h3 class="function {../@visibility}">
      <img src="{$root}images/icons/visibility_{../@visibility}.png" style="margin-right: 10px" alt="{@visibility}" title="{@visibility}"/>
      <xsl:value-of select="." />
    </h3>
      <div class="to-top"><a href="#top">jump to top</a></div>
  </xsl:template>

    <xsl:template match="argument">
        <xsl:variable name="name" select="name"/>
        <tr>
            <th>
                <xsl:value-of select="$name"/>
            </th>
            <td>
                <xsl:if test="../docblock/tag[@name='param' and @variable=$name]/type">
                    <xsl:call-template name="implodeTypes">
                        <xsl:with-param name="items" select="../docblock/tag[@name='param' and @variable=$name]/type"/>
                    </xsl:call-template>
                </xsl:if>
            </td>
            <td>
                <em>
                    <xsl:value-of select="../docblock/tag[@name='param' and @variable=$name]/@description" disable-output-escaping="yes"/>
                </em>
            </td>
        </tr>
    </xsl:template>

  <xsl:template match="function|method">
    <a id="{../full_name}::{name}()" class="anchor" />
    <div>
        <xsl:attribute name="class">
            <xsl:value-of select="concat(name(), ' ', @visibility)" />
            <xsl:if test="inherited_from"> inherited_from </xsl:if>
        </xsl:attribute>

        <a href="#" class="gripper">
            <img src="{$root}images/icons/arrow_right.png" />
            <img src="{$root}images/icons/arrow_down.png" style="display: none;"/>
        </a>

        <code class="title">
        <img src="{$root}images/icons/{name()}.png" alt="{name()}" title="{name()}" />
        <xsl:if test="@visibility">
            <img src="{$root}images/icons/visibility_{@visibility}.png" style="margin-right: 5px" alt="{@visibility}" title="{@visibility}"/>
        </xsl:if>
        <span class="highlight"><xsl:value-of select="name" /></span>
            <span class="nb-faded-text">(<xsl:for-each select="argument">
                <xsl:if test="position() &gt; 1">, </xsl:if><xsl:variable name="variable_name" select="name" />
                <xsl:call-template name="implodeTypes">
                    <xsl:with-param name="items" select="../docblock/tag[@name='param' and @variable=$variable_name]/type" />
                </xsl:call-template>&#160;<xsl:value-of select="$variable_name" />
                <xsl:if test="default != ''"> = <xsl:value-of select="default" disable-output-escaping="yes" /></xsl:if>
            </xsl:for-each>)</span>: <xsl:if test="not(docblock/tag[@name='return'])">void</xsl:if><xsl:apply-templates select="docblock/tag[@name='return']" />

            <xsl:if test="@static='true'">
                <span class="attribute">static</span>
            </xsl:if>

            <xsl:if test="@final='true'">
                <span class="attribute">final</span>
            </xsl:if>

            <xsl:if test="@abstract='true'">
                <span class="attribute">abstract</span>
            </xsl:if>

            <xsl:if test="inherited_from">
                <span class="attribute">inherited</span>
            </xsl:if>
        </code>

        <xsl:if test="count(docblock/tag[@name='Route'])">
            <dl class="param-route dl-horizontal">
                <xsl:call-template name="tagRoute">
                    <xsl:with-param name="str" select="docblock/tag[@name='Route']" />
                </xsl:call-template>
            </dl>
        </xsl:if>


        <div class="clear"></div>
    </div>

  </xsl:template>

</xsl:stylesheet>