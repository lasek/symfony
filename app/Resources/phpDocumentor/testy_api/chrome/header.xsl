<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">

    <xsl:template name="page-header">
        <h1>
            <xsl:if test="$title">
                <img src="{$root}images/logo.png" id="sidebar-logo" alt="Logo" />
                <xsl:value-of select="$title" disable-output-escaping="yes" />
            </xsl:if>
        </h1>
    </xsl:template>

</xsl:stylesheet>
