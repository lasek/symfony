<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
    <xsl:include href="chrome.xsl" />

    <xsl:template name="content">
        <div class="content">
            <br />
            <span class="created">Utworzone dnia: <xsl:value-of select="$generated_datetime" /></span>
            <h2>Dokumentacja do projektu Testów</h2>
            <p>Przeznaczona do oceny spójności w nazewnictwie plików i definicji metod.</p>
        </div>
        <div class="content">
            <ul>
                <li><span class="date">01.07.2014</span> - początek <strong>Projektu</strong></li>
            </ul>
        </div>
        <h3>Aktualna konfiguracja:</h3>
        <ul>
            <li>root: <xsl:value-of select="$version" /></li>
            <li>section.dashboard.show: <xsl:value-of select="$section.dashboard.show" />!</li>
        </ul>
        <h3>pauluZ</h3>
        <h3>pauluZ - jak nie widać to znaczy że zmienna powyżej się zacięła</h3>
    </xsl:template>

</xsl:stylesheet>
